package b137.tenio.s04d2;


import b137.tenio.s04d2.abstraction.AnotherPerson;
import b137.tenio.s04d2.abstraction.Person;
import b137.tenio.s04d2.inheritance.Animal;
import b137.tenio.s04d2.inheritance.Dog;
import b137.tenio.s04d2.polymorphism.ChildClass1;
import b137.tenio.s04d2.polymorphism.ChildClass2;
import b137.tenio.s04d2.polymorphism.DynamicPolymorphism;
import b137.tenio.s04d2.polymorphism.StaticPolymorphism;

public class Main {
    public static void main(String[] args) {

        Animal firstAnimal = new Animal("Nyebe", "white");


        firstAnimal.showDetails();

        Dog firstDog = new Dog("Angel", "brown", "caramelo");


        firstDog.bark();

        System.out.println(firstDog.getBreed());
        System.out.println(firstDog.getName());


        firstDog.showDetails();

        System.out.println();

        Person firstPerson = new Person();

        firstPerson.sleep();
        firstPerson.run();

        firstPerson.computerProgram();
        firstPerson.burp();

        System.out.println();

        AnotherPerson secondPerson = new AnotherPerson();

        secondPerson.sleep();
        secondPerson.run();
        secondPerson.computerProgram();
        secondPerson.burp();

        System.out.println();

        StaticPolymorphism poly = new StaticPolymorphism();

        System.out.println(poly.add(5,8));

        System.out.println(poly.add(6, 6, 6));

        System.out.println(poly.add(5.2,8.1));



        System.out.println();

        DynamicPolymorphism poly2;

        poly2 = new ChildClass1();
        poly2.message();

        poly2 = new ChildClass2();
        poly2.message();

        poly2 = new DynamicPolymorphism();
        poly2.message();


    }

}
