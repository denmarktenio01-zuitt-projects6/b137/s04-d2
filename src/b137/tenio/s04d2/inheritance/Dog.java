package b137.tenio.s04d2.inheritance;

public class Dog extends Animal {
    // Properties
    private String breed;

    // Constructor
    public Dog () {
        super();
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;

    }

    // Getters
    public String getBreed() {
        return this.breed;
    }

    // Setter
    public void setBreed(String newBreed) {
        this.breed = newBreed;
    }

    // Methods
    public void bark() {
        super.showDetails();
    }

    public void  showDetails() {
        System.out.println("I am a " + super.getName() + " , with color " + super.getColor() + ". My breed is " + this.breed);
    }


}
