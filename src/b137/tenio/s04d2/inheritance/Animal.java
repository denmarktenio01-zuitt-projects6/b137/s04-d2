package b137.tenio.s04d2.inheritance;

public class Animal {
    // Properties
    private String name;
    private String color;

    // Constructors
    public Animal() {

    }
    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }
    // Getters
    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }
    // Setters
    public void setName(String newName) {
        this.name = newName;
    }
    public void setColor(String newColor) {
        this.color = newColor;
    }

    // Methods
    public void  showDetails() {
        System.out.println("I am a " + this.name + " , with color " + this.color);
    }
}
