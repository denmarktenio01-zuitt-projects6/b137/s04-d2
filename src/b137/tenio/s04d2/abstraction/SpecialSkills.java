package b137.tenio.s04d2.abstraction;

public interface SpecialSkills {
    default void computerProgram() {
        System.out.println("I can code Java.");
    }
    default void burp() {
        System.out.println("I can burp at will.");
    }


}
