package b137.tenio.s04d2.abstraction;

public class Person implements Actions, SpecialSkills{

    public Person() {

    }

    // Action Methods
    public void sleep(){
        Actions.super.sleep();
    }

    public void run() {
        Actions.super.run();
    }

    // SpecialSkills Methods
    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }

    public void burp() {
        SpecialSkills.super.burp();
    }


}
