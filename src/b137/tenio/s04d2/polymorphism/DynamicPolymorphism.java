package b137.tenio.s04d2.polymorphism;

public class DynamicPolymorphism {

    public void message() {
        System.out.println("I am the parent class! Hiya!");
    }
}
