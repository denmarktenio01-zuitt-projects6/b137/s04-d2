package b137.tenio.s04d2.polymorphism;

public class StaticPolymorphism {
    public int add(int a, int b) {
        return a + b;
    }

    // overload add method by adding a parameter
    public int add(int a, int b, int c) {
        return a + b + c;
    }

    // overload by changing the datatype of a parameter
    public double add(double a, double b) {
        return a + b;
    }
}
